﻿using UnityEngine;
using System;
using System.Collections;
using LitJson;
using System.IO;

public class LaCura : MonoBehaviour 
{
	private WWW www;
	private Datas datas;
	public float _waitTime = 10;

	public Vector3 outCart;
	public GameObject myPrefab;
	private GameObject mappa;


	void Start () 
	{	
		StartCoroutine(MyCoroutine(_waitTime));

		if(mappa)
			Destroy(mappa);
	
	}

	public IEnumerator MyCoroutine(float waitTime)
	{
		yield return new WaitForSeconds(_waitTime);
		Debug.Log("Start call to loadjson every: " + waitTime + " sec.");
		Start();

		www = new WWW("http://la-cura.it/HE_CURA/API/getAllCoordsForResearch.php?w=cura");
		//convert the loaded string into an object of the JsonData type. We can use a static method of the JsonMapper class to achieve this:
		yield return www;
		if (www.error == null && www.isDone )
		{  //Sucessfully loaded the JSON string
			Debug.Log("Loaded following JSON string" + www.text);

			//Process JSON file
			LoadJsonLaCura();
		}
		else
		{
			Debug.Log("ERROR: " + www.error);
		}
	}

	void LoadJsonLaCura()
	{
		string jsonString = www.text;
		JsonData jsonGetAllCoordsForResearch = JsonMapper.ToObject(jsonString);
		//convert the loaded string into an object of the JsonData type. We can use a static method of the JsonMapper class to achieve this:

		datas = new Datas();
		print("jsonGetAllCoordsForResearch: " + jsonGetAllCoordsForResearch.Count);

		int n = jsonGetAllCoordsForResearch.Count;
		GameObject[] utenti = new GameObject[n];
		mappa = new GameObject("Mappa");

		for(int i = 0; i < jsonGetAllCoordsForResearch.Count; i++)
		{
			datas.latitudine = jsonGetAllCoordsForResearch[i]["lat"].ToString();
			datas.longitudine = jsonGetAllCoordsForResearch[i]["lng"].ToString();

			float _flat = (float)Convert.ToDouble(datas.latitudine);
			float _flong = (float)Convert.ToDouble(datas.longitudine);

			SphericalToCartesian(1000, _flong, _flat, 0, out outCart);

			utenti[i] = Instantiate(myPrefab, transform.localScale, Quaternion.identity) as GameObject;
			utenti[i].transform.parent = mappa.transform;
			utenti[i].transform.localPosition = outCart;
			//print("n. " + i + " Latitudine: " + datas.latitudine + " Longitudine: " + datas.longitudine + " | "  );
		}

	}
		

	public static void SphericalToCartesian(float radius, float lon, float lat, float z1, out Vector3 outCart)
	{
		// convert cartesian to polar system
		float a = radius * Mathf.Cos(Mathf.Deg2Rad*lat);
		outCart.x = a * Mathf.Cos(Mathf.Deg2Rad*(lon));
		outCart.z = a * Mathf.Sin(Mathf.Deg2Rad*(lon));
		outCart.y = radius * Mathf.Sin(Mathf.Deg2Rad*lat);// - (z1/6371* radius);	
	}


	public class Datas 
	{   
		//public int id;
		public string latitudine;
		public string longitudine;

	}
}

