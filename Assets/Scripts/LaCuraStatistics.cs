﻿/*
 * tools-getProjectStatistics.php?w=[research-code]
 * 
 * Gives back a JSON file with some statistics for [research-code] 
 * (number of contents captured, number of users, number of geolocated contents, distribution of emotions).
 * 
 * http://human-ecosystems.com/HE_BO/API/tools-getProjectStatistics.php?w=bologna
 * 
 * unity parse JSON tutorial reference : http://blog.paultondeur.com/2010/03/23/tutorial-loading-and-parsing-external-xml-and-json-files-with-unity-part-2-json/
 * 
 * 
 * 
*/

using UnityEngine;
using System;
using System.Collections;
using LitJson;
using System.IO;

public class LaCuraStatistics : MonoBehaviour 
{
	private string HE_BO = "http://human-ecosystems.com/HE_BO/API/";
	private string getStatistics = "tools-getProjectStatistics.php?w=bologna";
	private string fullStringEmotions;
	private float waitTime = 10.0f;
	private WWW www;
	private Datas datas;
	private GameObject[] emotions;
	public GameObject myPrefab;
	private GameObject temp;
	public Color32[] colorsEmotion;
	private float radius = 400.0f;
	public Gradient gradient;

	void Awake()
	{
		colorsEmotion = new Color32[12];
		for(int i = 0; i < colorsEmotion.Length; i++)
		{
			colorsEmotion[i].r = (byte)UnityEngine.Random.Range(0,255);
			colorsEmotion[i].g = (byte)UnityEngine.Random.Range(0,255);
			colorsEmotion[i].b = (byte)UnityEngine.Random.Range(0,255);
			colorsEmotion[i].a = 255;
		}
	}

	IEnumerator Start () 
	{
		fullStringEmotions = Path.Combine(HE_BO, getStatistics);
		//StartCoroutine(MyCoroutine(_waitTime));
		if(temp)
			Destroy(temp);

		www = new WWW(fullStringEmotions);
		//convert the loaded string into an object of the JsonData type. We can use a static method of the JsonMapper class to achieve this:
		yield return www;
		if (www.error == null && www.isDone )
		{  //Sucessfully loaded the JSON string
			Debug.Log("Loaded following JSON string" + www.text);

			//Process JSON file
			LoadJsonLaCuraStatistics();
		}
		else
		{
			Debug.Log("ERROR: " + www.error);
		}
	}


	void LoadJsonLaCuraStatistics()
	{
		string jsonString = www.text;
		JsonData emotionsObject = JsonMapper.ToObject(jsonString);
		//convert the loaded string into an object of the JsonData type. We can use a static method of the JsonMapper class to achieve this:

		datas = new Datas();
		print("Emotions: " + emotionsObject["emotions"].Count);

		int numEmo = emotionsObject["emotions"].Count;
		GameObject[] emotions = new GameObject[numEmo];


		temp = new GameObject("temp");
		for(int i = 0; i < emotionsObject["emotions"].Count; i++)
		{
			//float angle = i * Mathf.PI * 2 / emotionsObject["emotions"].Count;

			float[] values = new float[numEmo];
			values[i] = float.Parse(emotionsObject["emotions"][i]["value"].ToString()) / 100;
			print(emotionsObject["emotions"][i]["label"].ToString() +  values[i]);

			Vector3 gapPos = new Vector3(i * 82, 0, 0);

			//Vector3 posCircle = new Vector3 (Mathf.Cos(angle), 0, Mathf.Sin(angle)) * radius;
		
			emotions[i] = Instantiate(myPrefab, gapPos, Quaternion.Euler(Vector3.forward)) as GameObject;
			emotions[i].transform.parent = temp.transform;
			emotions[i].transform.localScale = new Vector3(40, values[i], 40);
			colorsEmotion = new Color32[13];
			colorsEmotion[i].r = (byte)UnityEngine.Random.Range(0,255);
			colorsEmotion[i].g = (byte)UnityEngine.Random.Range(0,255);
			colorsEmotion[i].b = (byte)UnityEngine.Random.Range(0,255);
			colorsEmotion[i].a = 255;

			emotions[i].GetComponent<Renderer>().material.color = new Color32( colorsEmotion[i].r, colorsEmotion[i].g, colorsEmotion[i].b, colorsEmotion[i].r );
			/*Texture2D tex = new Texture2D(13, 1);

			emotions[i].GetComponent<Renderer>().material.mainTexture = tex;

			for(int pix = 0; pix < tex.width; pix++)
			{
				tex.SetPixels32(colorsEmotion);
				tex.SetPixel(pix, 1, new Color(UnityEngine.Random.Range(0,255), UnityEngine.Random.Range(0,255), UnityEngine.Random.Range(0,255)));
			}
				
			tex.Apply();
			*/
			 
		}
			
	}

	public class Datas 
	{   
		//public int id;
		public string latitudine;
		public string longitudine;

	}

}
